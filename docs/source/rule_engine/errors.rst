:mod:`errors`
=============

.. module:: rule_engine.errors
   :synopsis:

This module contains the exceptions raised by the package.

Exceptions
----------

.. autoexception:: DatetimeSyntaxError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: EngineError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: EvaluationError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: RegexSyntaxError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: RuleSyntaxError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: SymbolResolutionError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: SymbolTypeError
   :members:
   :show-inheritance:
   :special-members: __init__

.. autoexception:: SyntaxError
   :members:
   :show-inheritance:
   :special-members: __init__
